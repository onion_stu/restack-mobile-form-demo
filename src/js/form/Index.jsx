import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import mobiscroll, { Form } from "mobiscroll"
import Input from "./components/Input"
import  './style/form.less'

@connect(
  state => {
    return {
    }
  }
)
@withRouter
export default class FormDemo extends Component {

  static propTypes = {

  }

  render() {
    // var mailReg = /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/
    // var mailReg = /^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/
    var mailReg = /^([\w-]{1,})@([\w-]{1,}\.)(\w.{1,})$/
    var numReg = /^[0-9]*$/
    return (
      <div className="form-demo">
        <Form theme="mobiscroll" lang="zh">
          <Input rules={[{reg:mailReg,msg:'请输入正确的邮箱'}]} fieldName="email" labelText="Email" />
          <Input rules={[{reg:numReg,msg:'请输入数字'}]} labelText="Number" />
        </Form>
      </div>
    );
  }

}

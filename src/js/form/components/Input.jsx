import React, {Component, PropTypes} from 'react'
import _ from 'lodash'
import classNames from 'classnames'

 class Input extends Component {

  constructor(props) {
    super(props);
    const { rules } = props
    let stateRules = []
    if(!_.isEmpty(rules)){
      if(_.isArray(rules)&&rules.length>0){
        stateRules = rules
      }else if(_.isObject(rules)){
        stateRules = [rules]
      }
    }
    console.warn(stateRules)
    this.state = {
      errMsg: '',
      rules:stateRules
    }
  }

  componentWillReceiveProps(nextProps){

  }

  componentDidMount() {

  }
  componentWillUnmount() {

  }

  onInputChange = (event) => {
    const { rules } = this.state
    let el = event.currentTarget;
    let errMsg = "";
    for(let i = 0, len = rules.length;i<rules.length;i++){
      let rule = rules[i];
      let reg = rule.reg;
      if(reg&&!reg.test(el.value)){
        errMsg = rule.msg
        break;
      }
    }
    this.setState({errMsg})
  }

  render() {
    const { labelText, className, fieldName } = this.props
    const { errMsg } = this.state

    let wapperClass = classNames('mbsc-input mbsc-control-w',className,{'mbsc-err':!!errMsg})

    return (
      <div className={wapperClass}>
        {
          labelText&&<label htmlFor={fieldName}>{labelText}</label>
        }
        <input id={fieldName} name={fieldName} type="text" placeholder="Text field label" onChange={this.onInputChange}/>
        {
          errMsg&&<span className="mbsc-err-msg">{errMsg}</span>
        }
      </div>
    );
  }

}
Input.propTypes = {};
export default Input

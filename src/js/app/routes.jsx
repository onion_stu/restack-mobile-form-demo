import React from 'react'
import {Route, IndexRoute} from 'react-router'

import App from './App'
import Form from '../form/Index'

const rootRoute = (
  <Route path="/" component={App}>
    <IndexRoute component={Form}/>
  </Route>
)

export default rootRoute
